module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      // predefined color codes
      colors: {
        'orangered': '#ff4500',
        'tangelo' : '#f94d00',
        'papayawhip' : '#ffefd5',
        'teracotta' : '#e2725b'
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
        Kumbh: ['Kumbh Sans', 'sans-serif'],
      },
    },
  },
  plugins: [],
}
