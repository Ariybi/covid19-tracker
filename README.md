This project is created with ❤. By Ariyibi Baseet Adekunle

My first project created with VUE 3

----------------------------- TECHNOLOGIES USED ----------------------------------------------:
-- HTML5
-- CSS3
-- JAVASCRIPT
-- VUE 3 + VITE
-- TAILWIND CSS
-- GIT
-- BITBUCKET

ABOUT THE PROJECT:
COVID19 TRACKER is created to keep track of Covid cases across the world

# API USED #
https://api.covid19api.com
